exports.blogPosts = [
    {
      id: 1,
      title: "My First Blog Post",
      content: "This is the content of my first blog post.",
      author: "John Doe",
      created_at: new Date()
    },
    {
      id: 2,
      title: "The Benefits of Yoga",
      content: "Yoga has been shown to reduce stress, improve flexibility, and increase mindfulness.",
      author: "Jane Smith",
      created_at: new Date()
    },
    {
      id: 3,
      title: "The Importance of Sleep",
      content: "Getting enough sleep is crucial for overall health and well-being.",
      author: "Mike Johnson",
      created_at: new Date()
    },
    {
      id: 4,
      title: "10 Tips for Staying Productive While Working from Home",
      content: "Working from home can be challenging, but these tips can help you stay productive and focused.",
      author: "Sara Lee",
      created_at: new Date()
    },
    {
      id: 5,
      title: "The Benefits of Meditation",
      content: "Meditation has been shown to reduce anxiety, improve focus, and increase feelings of well-being.",
      author: "David Kim",
      created_at: new Date()
    }
  ];
  