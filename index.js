const express = require('express');
const app = express();
const { blogPosts } = require('./posts')


app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello, World!');
})

// 전체 포스팅 가져오기
app.get('/posts', (req, res) => {
    const postsList = blogPosts.map((post) => {
        const { title, content, author} = post;
        return {
            title, content, author
        }
    });

    if (blogPosts.length < 1) {
        res.send('There is no post.');
        return;
    }
    res.send(postsList);
})

// 작가별 포스팅 가져오기
app.get('/posts/author/:author', (req, res, next) => {
    const { author } = req.params;
    // posts author 이름에 spacing이 있어서 아래 부분을 처리해줘야 함...하지만 넘어감.
    // const authorName = decodeURIComponent(req.params.author.replace(/-/g, ' '));
    const postsByAuthor = blogPosts.filter((post) => post.author == author);
    if (!postsByAuthor) {
        res.send('There is no post for the author.');
    }
    res.send(postsByAuthor);
})

// 포스팅 상세 가져오기
app.get('/posts/:id', (req, res) => {
    const { id } = req.params;
    const postsById = blogPosts.filter((post) => post.id == id );
    
    if (!postsById) {
        res.send('There is no such post with the id.');
    }

    res.send(postsById);
})

// 새로운 포스트 작성하기
app.post('/posts', (req, res) => {
    const { id, title, content, author } = req.body;
    const created_at = new Date();

    blogPosts.push({
        id, title, content, author, created_at
    });

    res.send('New post is created!');
});

app.put('/posts/:id', (req, res) => {
    const { id } = req.params;
    const { title, content } = req.body;

    const idx = blogPosts.findIndex((post) => post.id == id);
    if (idx < 0) res.send('there is no such post with the id.');

    const willUpdate = blogPosts[idx];
    willUpdate.title = title;
    willUpdate.content = content;
    
    res.send('This Post is updated.');
});

app.delete('/posts/:id', (req, res) => {
    const { id } = req.params;
    const idx = blogPosts.findIndex((post) => post.id == id);
    
    if (idx < 0) res.send('There is no such post with the id.');
    blogPosts.splice(idx, 1);
    res.send('successfully deleted.');
})

app.listen(8080, () => {
    console.log('The Server is successfully Running at PORT 8080');
})